package com.example.chucknorris.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ChuckNorrisService {

    String RANDOM_JOKE = "jokes/random";
    String CATEGORIES = "jokes/categories";

    @GET(RANDOM_JOKE)
    Call<ChuckNorrisJoke> getRandomJoke();

    @GET (CATEGORIES)
    Call<String[]> getCategories();

    /*
    @GET(RANDOM_JOKE+"category")
    Call<ChuckNorrisJoke> getJokeByCategory(@Query("category") String chosenCategory);


     */

    @GET (RANDOM_JOKE)
    Call<ChuckNorrisJoke> getJokeByCategory(@Query("category") String chosenCategory);

    //el @QUERY fa això:    ?category={category}


    //https://api.chucknorris.io/jokes/random?category={category}


}

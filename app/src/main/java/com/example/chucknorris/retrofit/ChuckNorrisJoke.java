package com.example.chucknorris.retrofit;

import com.google.gson.annotations.SerializedName;

public class ChuckNorrisJoke {

    @SerializedName("id")
    private String id;

    @SerializedName("value")
    private String value;

    public ChuckNorrisJoke(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}

package com.example.chucknorris.retrofit;

import android.app.Application;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoryChuck {

    private static ChuckNorrisService chuckNorrisService;
    private static Call<String[]> categories;
    private static String[] options;


    public RepositoryChuck() {

        RetrofitChuckNorrisInstance.getRetrofit();

    }

    public static void enable() {

        chuckNorrisService = RetrofitChuckNorrisInstance.getRetrofit().create(ChuckNorrisService.class);

        categories = chuckNorrisService.getCategories();
        categories.enqueue(new Callback<String[]>() {
            @Override
            public void onResponse(Call<String[]> call, Response<String[]> response) {

                options = response.body();

            }

            @Override
            public void onFailure(Call<String[]> call, Throwable t) {

            }
        });
    }


    public static String[] getOptions() {
        return options;
    }
}

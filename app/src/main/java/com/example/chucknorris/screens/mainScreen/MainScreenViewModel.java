package com.example.chucknorris.screens.mainScreen;

import android.app.AlertDialog;
import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.chucknorris.retrofit.ChuckNorrisJoke;
import com.example.chucknorris.retrofit.ChuckNorrisService;
import com.example.chucknorris.retrofit.RepositoryChuck;
import com.example.chucknorris.retrofit.RetrofitChuckNorrisInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainScreenViewModel extends AndroidViewModel {

    ChuckNorrisService chuckNorrisService;
    Call<String[]> categories;
    private String[] options;


    private RepositoryChuck repositoryChuck;
    private LiveData<ChuckNorrisJoke> chuckNorrisJokeLiveData;


    public MainScreenViewModel(Application application) {
        super(application);
        repositoryChuck = new RepositoryChuck();
    }


    LiveData<ChuckNorrisJoke> getJoke() {
        return chuckNorrisJokeLiveData;
    }


    public void enableRepository() {
        RepositoryChuck.enable();

    }

    public String[] getCategories() {
       options = RepositoryChuck.getOptions();
       return options;
    }
}


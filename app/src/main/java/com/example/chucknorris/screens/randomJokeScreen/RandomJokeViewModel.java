package com.example.chucknorris.screens.randomJokeScreen;

import androidx.lifecycle.ViewModel;

import com.example.chucknorris.retrofit.ChuckNorrisService;
import com.example.chucknorris.retrofit.RetrofitChuckNorrisInstance;

public class RandomJokeViewModel extends ViewModel {

    ChuckNorrisService chuckNorrisService = RetrofitChuckNorrisInstance.getRetrofit().create(ChuckNorrisService.class);

    public ChuckNorrisService getRetrofitInstance() {
        return chuckNorrisService;
    }
    // TODO: Implement the ViewModel
}

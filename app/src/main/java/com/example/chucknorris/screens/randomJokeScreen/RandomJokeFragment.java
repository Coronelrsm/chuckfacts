package com.example.chucknorris.screens.randomJokeScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.chucknorris.R;
import com.example.chucknorris.retrofit.ChuckNorrisJoke;
import com.example.chucknorris.retrofit.ChuckNorrisService;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RandomJokeFragment extends Fragment {


    @BindView(R.id.chuckJoke)
    TextView chuckJoke;
    @BindView(R.id.btnAnotherJoke)
    Button btnAnotherJoke;
    @BindView(R.id.chuckPicture)
    ImageView chuckPicture;
    private RandomJokeViewModel mViewModel;

    private int[] photos = {R.drawable.chuck1, R.drawable.chuck2, R.drawable.chuck3, R.drawable.chuck4,
            R.drawable.chuck5, R.drawable.chuck6, R.drawable.chuck7, R.drawable.chuck8, R.drawable.chuck9,
            R.drawable.chuck10, R.drawable.chuck11, R.drawable.chuck12, R.drawable.chuck13, R.drawable.chuck14,
            R.drawable.chuck15, R.drawable.chuck16, R.drawable.chuck17, R.drawable.chuck18, R.drawable.chuck19,

    };

    private ChuckNorrisService chuckNorrisService;
    Call<ChuckNorrisJoke> chuckJokeCall;
    ChuckNorrisJoke chuckNorrisJoke;

    public static RandomJokeFragment newInstance() {
        return new RandomJokeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.random_joke_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RandomJokeViewModel.class);

        displayJoke();

    }

    private void displayJoke() {

        chuckNorrisService = mViewModel.getRetrofitInstance();
        chuckJokeCall = chuckNorrisService.getRandomJoke();
        chuckJokeCall.enqueue(new Callback<ChuckNorrisJoke>() {
            @Override
            public void onResponse(Call<ChuckNorrisJoke> call, Response<ChuckNorrisJoke> response) {
                chuckNorrisJoke = response.body();


                Random ran=new Random();

                int i=ran.nextInt(photos.length);
                chuckPicture.setImageResource(photos[i]);


                chuckJoke.setText(chuckNorrisJoke.getValue());

            }

            @Override
            public void onFailure(Call<ChuckNorrisJoke> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.btnAnotherJoke)
    public void onViewClicked() {

        displayJoke();
    }
}


/*

ImageView image = (ImageView) findViewById(R.id.imageview1);

Random ran=new Random();
int i=ran.nextInt(photos.length);
image.setImageResource(photos[i]);
image.setOnClickListener(new View.onClickListener()
{
    public void onClick(View v)
    {
      int k=ran.nextInt(photos.length);
      image.setImageResource(photos[k]);
    }
}
);
 */
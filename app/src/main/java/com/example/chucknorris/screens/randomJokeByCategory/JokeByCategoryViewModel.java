package com.example.chucknorris.screens.randomJokeByCategory;

import androidx.lifecycle.ViewModel;

import com.example.chucknorris.retrofit.ChuckNorrisService;
import com.example.chucknorris.retrofit.RetrofitChuckNorrisInstance;

public class JokeByCategoryViewModel extends ViewModel {

    ChuckNorrisService chuckNorrisService = RetrofitChuckNorrisInstance.getRetrofit().create(ChuckNorrisService.class);

    public ChuckNorrisService getRetrofitInstance() {
        return chuckNorrisService;
    }
}

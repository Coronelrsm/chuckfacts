package com.example.chucknorris.screens.mainScreen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.example.chucknorris.R;
import com.example.chucknorris.retrofit.ChuckNorrisJoke;
import com.example.chucknorris.retrofit.ChuckNorrisService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainScreenFragment extends Fragment {

    @BindView(R.id.btnNavigateToRandomJoke)
    Button btnNavigateToRandomJoke;
    @BindView(R.id.btnSelectCategory)
    Button btnSelectCategory;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.btnNavigateJokeCategory)
    Button btnNavigateJokeCategory;
    private MainScreenViewModel mViewModel;
    private View globalview;
    private String[] options;
    private String chosenCategory;



    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_screen_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        globalview = view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);
        mViewModel.enableRepository();

    }


    private void navigateToRandomJoke() {
        NavDirections action = MainScreenFragmentDirections.navigateToRandomJoke();
        Navigation.findNavController(globalview).navigate(action);
    }

    @OnClick({R.id.btnNavigateToRandomJoke, R.id.btnSelectCategory, R.id.btnNavigateJokeCategory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnNavigateToRandomJoke:
                navigateToRandomJoke();
                break;
            case R.id.btnSelectCategory:
                displayMenu();
                break;
            case R.id.btnNavigateJokeCategory:
                navigateToJokeByCategory();
                break;
        }
    }


    void displayMenu() {
        options = mViewModel.getCategories();
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle("Choose a category");

        int checkedItem = 1;
        builder.setSingleChoiceItems(options, checkedItem, (dialog, which) -> {

            chosenCategory = options[which]; //which és la opció dins l'array de Strings, és la categoria que triem
            category.setText(chosenCategory);
        });

        builder.setPositiveButton("OK", (dialog, which) -> {
        });
        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();


    }

    private void navigateToJokeByCategory() {
        NavDirections action = MainScreenFragmentDirections.navigateToJokeByCategory(chosenCategory);
        Navigation.findNavController(globalview).navigate(action);
    }


}
